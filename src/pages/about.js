import * as React from 'react'
import { StaticImage } from 'gatsby-plugin-image'
import Layout from '../components/layout'
import '../styles/global.css'

const About = () => {
  return (
    <Layout pageTitle="About">
      <h1 className='blog-title'>About</h1>
      <StaticImage
        className='img'
        src='../images/about.jpg'
        alt='Travel'
      />
      <p>Praesent iaculis nunc libero, vel eleifend lectus tempor a. Nulla quis nulla sem. In eu placerat leo. 
        Sed in leo lectus. Donec velit felis, fermentum id tortor quis, euismod hendrerit enim. Integer id 
        rhoncus quam. Ut vel sapien id lectus fermentum malesuada non ut ante. Sed dapibus quis lorem eu laoreet. 
        Morbi et sapien felis. Praesent in risus vel erat faucibus venenatis. Proin sit amet luctus nisl. Sed 
        ullamcorper porta risus, feugiat tristique nibh sagittis non. Fusce placerat urna dolor, eget venenatis 
        lorem tincidunt at. Vestibulum in eros lobortis, interdum leo ut, dapibus diam.</p>

      <p>Sed quis nulla sollicitudin, rhoncus libero convallis, auctor orci. Aliquam et porttitor enim. Aliquam 
        vehicula velit eu urna viverra, id interdum odio sodales. Aenean laoreet sed augue at efficitur. Aenean 
        ultricies diam risus, et suscipit tortor ullamcorper eu. Proin gravida eu tellus eu eleifend. Cras ac 
        facilisis sapien. Cras id augue in urna pellentesque posuere.</p>
    </Layout>
  )
}

export default About
