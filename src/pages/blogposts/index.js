import * as React from 'react'
import { graphql, Link } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
import Layout from '../../components/layout'
import '../../styles/global.css'
import {
    posts,
    indivPost,
    text,
} from '../../styles/blog.module.css'

const Blog = ({ data }) => {
  return (
    <Layout pageTitle="Blogposts" className='overflow'>
      <h1 className='blog-title'>Blog</h1>
      <div className={posts}>
        {data.allMdx.nodes.map( node => (
          <article key={node.id} className={indivPost}>
            <Link to={`/blogposts/${node.frontmatter.slug}`}>
              <StaticImage
                src='../../images/location-pin.png'
                alt='Location Pin'
              />
              <div  className={text}>
                <h2>{node.frontmatter.title}</h2>
                <p>Posted: {node.frontmatter.date}</p>
              </div>
            </Link>
          </article>
        ))}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allMdx(sort: {fields: frontmatter___date, order: DESC}) {
      nodes {
        frontmatter {
          title
          slug
          date(formatString: "MMMM D, YYYY")
        }
        id
      }
    }
  }
`

export default Blog
