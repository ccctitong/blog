import * as React from 'react'
import { graphql } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import Layout from '../../components/layout'
import '../../styles/global.css'

const Post = ({ data }) => {
    const image = getImage(data.mdx.frontmatter.hero_image)

  return (
    <Layout pageTitle={data.mdx.frontmatter.title}>
        <h1 className='blog-title'>{data.mdx.frontmatter.title}</h1>
        <GatsbyImage
            className='img'
            image={image}
            alt={data.mdx.frontmatter.hero_image_alt}
        />
        <MDXRenderer>
            {data.mdx.body}
        </MDXRenderer>
        <p className='date'>Posted: {data.mdx.frontmatter.date}</p>
    </Layout>
  )
}

export const query = graphql`
    query ($id: String) {
        mdx(id: {eq: $id}) {
            body
            frontmatter {
                title
                date(formatString: "MMMM D, YYYY")
                hero_image_alt
                hero_image {
                    childImageSharp {
                        gatsbyImageData
                    }
                }
            }
        }
    }
`

export default Post
