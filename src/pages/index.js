import * as React from 'react'
import { Link } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
import Layout from '../components/layout'
import '../styles/global.css'
import {
    container,
    header,
    text,
    button,
    image,
} from '../styles/hompage.module.css'

const IndexPage = () => {
  return (
    <Layout pageTitle="Home">
      <div className={container}>
        <div>
          <h1 className={header}>Lorem ipsum dolor<br/>sit amet.</h1>
          <p className={text}>Pellentesque sit amet purus sit amet nisi eleifend varius et ornare<br/>velit mauris sit amet lectus sit amet turpis rutrum varius</p>
          <Link to="/blogposts" className={button}>Explore with me</Link>
        </div>

        <StaticImage
          className={image}
          src='../images/home-image.png'
          alt='Travel'
        />
      </div>
    </Layout>
  )
}

export default IndexPage
