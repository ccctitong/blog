import * as React from 'react'
import { Link , useStaticQuery, graphql } from 'gatsby'
import '../styles/global.css'
import {
    wrapper,
    container,
    content,
    navbar,
    title,
    navItems,
    navLinks,
    footer,
} from '../styles/layout.module.css'

const Layout = ({ pageTitle, children }) => {
    const data = useStaticQuery(graphql`
        query {
            site {
                siteMetadata {
                    title
                }
            }
        }
    `)

    return (
        <div>
            <div className={wrapper}>
            <title>{pageTitle} | {data.site.siteMetadata.title}</title>
            <nav className={navbar}>
                <h1 className={title}>{data.site.siteMetadata.title}</h1>
                <div className={navItems}>
                    <Link to="/" className={navLinks}>Home</Link>
                    <Link to="/about" className={navLinks}>About</Link>
                    <Link to="/blogposts" className={navLinks}>Blog</Link>
                </div>
            </nav>
            <main className={container}>
                <div className={content}>
                    {children}
                </div>
            </main>
            </div>
            <footer className={footer}>
                Copyright © 2021 Blog.
            </footer>
        </div>
    )
}

export default Layout